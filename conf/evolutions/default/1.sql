# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table user_account (
  id                        bigint not null,
  account                   varchar(255),
  name                      varchar(255),
  pass                      varchar(255),
  de_tai                    varchar(255),
  description               varchar(255),
  date                      timestamp,
  email                     varchar(255),
  constraint pk_user_account primary key (id))
;

create sequence user_account_seq;




# --- !Downs

drop table if exists user_account cascade;

drop sequence if exists user_account_seq;

