package assets;

import controllers.Application;
import models.UserAccount;
import controllers.UserAccounts;
import play.GlobalSettings;
import com.avaje.ebean.Ebean;

import models.*;
import play.Logger;
import play.libs.Yaml;
import java.util.List;
import java.util.Map;
/**
 * Created by nguyenlongtn159 on 3/23/2015.
 */
public class Global extends GlobalSettings {

    public void onStart(Application app) {
        Logger.info("Application has started");
        InitialData.insert(app);
    }

    static class InitialData {
        public static void insert(Application app) {
            if (Ebean.find(UserAccount.class).findRowCount() == 0) {
                Map<String, List<Object>> all =
                        (Map<String, List <Object>>)Yaml.load("initial-data.yml");
                Ebean.save(all.get("accs"));
            }
        }
    }

    public void onStop(Application app) {
        Logger.info("Application shutdown...");
    }

}

