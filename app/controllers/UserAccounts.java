package controllers;

/**
 * Created by nguyenlongtn159 on 3/21/2015.
 */
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Page;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import models.UserAccount;
import play.mvc.Security;
import views.html.accs.details;
import views.html.accs.list;

import java.util.List;

//@Security.Authenticated(Secured.class)
public class UserAccounts extends Controller{

    private static final Form<UserAccount> accForm = Form.form(UserAccount.class);


    public static Result list(Integer page){
        Page<UserAccount> accs = UserAccount.find(page);
       // return ok(views.html.accs.list.render(accs));
        return ok(list.render(accs));

    }
    public static Result newUser(){
        return ok(details.render(accForm));
    }
    public static Result details(String account){
        final UserAccount product = UserAccount.findByAccount(account);
        if (product == null) {
            return notFound(String.format("Product %s does not exist.", account));
        }
        Form<UserAccount> filledForm = accForm.fill(product);
        return ok(details.render(filledForm));
    }

    public static Result save() {
        Form<UserAccount> boundForm = accForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("error", "Please correct the form below.");
            return badRequest(details.render(boundForm));
        }
        UserAccount acc = boundForm.get();
        //acc.save();
        if (acc.id == null) {
            if(UserAccount.findByAccount(acc.account) == null)
                acc.save();

    } else {
            acc.update();
    }

        flash("success", String.format("Successfully added product %s", acc));
        return redirect(routes.UserAccounts.list(0));
    }
    public static Result delete(String account) {
        final UserAccount acc = UserAccount.findByAccount(account);
        if(acc == null) {
            return notFound(String.format("Product %s does not exists.", account));
        }

        acc.delete();
        return redirect(routes.UserAccounts.list(0));
    }
}
