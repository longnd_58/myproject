package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.avaje.ebean.Page;
import play.data.format.Formats;
import play.data.validation.Constraints;
import views.html.accs.*;

import controllers.UserAccounts;
import play.db.ebean.Model;
import javax.persistence.Entity;
import javax.persistence.Id;
/**
 * Created by nguyenlongtn159 on 3/21/2015.
 *
 */  //danh dau day la 1 thuc the
@Entity
public class UserAccount extends Model{


//doc du lieu tu data
    public static Finder<Long,UserAccount> find =
            new Finder<Long,UserAccount>(Long.class, UserAccount.class);
// danh dau khoa chinh
    @Id
    public Long id;

    @Constraints.Required
    public String account;

    @Constraints.Required  // khong duoc trong
    public String name;

    @Constraints.Required
    public String pass;

    public String deTai;
    public String description;

    @Formats.DateTime(pattern = "MM-dd-yyyy")
    public Date date;

    @Constraints.Required
    public String email;

    public UserAccount() {
    }
    public UserAccount(String email, String pass) {
        this.email = email;
        this.pass = pass;
    }
    public UserAccount(String acc,String email , String pass, String name, String deTai, String des, Date date) {

        this.account = acc;
        this.name = name;
        this.pass= pass;
        this.deTai = deTai;
        this.description = des;
        this.date = date;
        this.email = email;
    }

    public String toString() {
        return String.format("%s - %s - %s - %s", account, email, name, pass);
    }
    public static List<UserAccount> findAll() {
        return find.all(); // sua de chi tim du lieu trong data
    }

    public static UserAccount findByAccount(String account) {
        return find.where().eq("account", account).findUnique(); //tim trong details goi, tim trong cac acc , tra ve 1 kq duy nhat
    }

    public static List<UserAccount> findByName(String term) {
        final List<UserAccount> accs = new ArrayList<UserAccount>();
        for (UserAccount candidate : accs) {
            if (candidate.name.toLowerCase().contains(term.toLowerCase())) {
                accs.add(candidate);
            }
        }
        return accs;
    }

    public static Page<UserAccount> find(int page) {  // trả về trang thay vì List
        return find.where()
                .orderBy("id asc")     // sắp xếp tăng dần theo id
                .findPagingList(5)    // quy định kích thước của trang
                .setFetchAhead(false)  // có cần lấy tất cả dữ liệu một thể?
                .getPage(page);    // lấy trang hiện tại, bắt đầu từ trang 0
    }
    public static UserAccount authenticate(String email, String pass) {
        return finder.where().eq("email", email)
                .eq("pass", pass).findUnique();
    }
    public static Finder<Long, UserAccount> finder =
            new Finder<Long, UserAccount>(Long.class, UserAccount.class);

   // public static boolean remove(UserAccount acc) {
  //      return accs.remove(acc);
   // }

   // public void save() {
    //    accs.remove(findByAccount(this.account));
   //     accs.add(this);
  //  }
}


